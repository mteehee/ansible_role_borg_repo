import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_borgbackup(host):
    ps = host.run("su '/usr/local/borgbackup/bin/borgbackup.sh' borgbackup")
    ps.rc
